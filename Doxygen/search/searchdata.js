var indexSectionsWithContent =
{
  0: "abcefghijlmnoprstvwx",
  1: "abgmpv",
  2: "ox",
  3: "abgmoprstv",
  4: "acegijmsv",
  5: "m",
  6: "cefhjlnptw",
  7: "clmp",
  8: "r"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "properties",
  7: "events",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Properties",
  7: "Events",
  8: "Pages"
};

