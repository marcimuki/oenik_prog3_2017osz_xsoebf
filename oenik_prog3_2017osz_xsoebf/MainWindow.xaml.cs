﻿namespace oenik_prog3_2017osz_xsoebf
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private ViewModel VM;

        public MainWindow()
        {
            this.InitializeComponent();
            this.VM = new ViewModel();
            this.DataContext = this.VM;
            this.KeyDown += this.MainWindow_KeyDown;
        }

        /// <summary>
        /// Billentyűlenyomás hatására meghívja a játékos és az ellenségek mozgását.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: this.VM.EllensegMozog(-1, 0); this.VM.JatekosMozog(-1, 0); break;
                case Key.Right: this.VM.EllensegMozog(1, 0); this.VM.JatekosMozog(1, 0);  break;
                case Key.Up: this.VM.EllensegMozog(0, -1); this.VM.JatekosMozog(0, -1);  break;
                case Key.Down: this.VM.EllensegMozog(0, 1); this.VM.JatekosMozog(0, 1);  break;
            }
        }
    }
}