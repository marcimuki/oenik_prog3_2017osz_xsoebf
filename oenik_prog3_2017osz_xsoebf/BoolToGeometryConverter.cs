﻿/// <summary>
/// Bool mátrixból Geometrybe alakít
/// </summary>

namespace oenik_prog3_2017osz_xsoebf
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;

    public class BoolToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Bool mátrixból Geometrybe alakít
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>Falak/ padlók GeometryGroupja</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool[,] m = (bool[,])value;
            GeometryGroup gg = new GeometryGroup();
            for (int i = 0; i < m.GetLength(0); i++)
            {
                for (int j = 0; j < m.GetLength(1); j++)
                {
                    if (!m[i, j])
                    {
                        gg.Children.Add(new RectangleGeometry(new System.Windows.Rect(j * Game.MEZO, i * Game.MEZO, Game.MEZO, Game.MEZO)));
                    }
                }
            }

            return gg;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
