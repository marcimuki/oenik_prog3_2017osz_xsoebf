﻿namespace oenik_prog3_2017osz_xsoebf
{
    using System.Windows;

    public class Ellenseg
    {
        private Point[] poziciok;
        private int ellensegIndex;

        public Ellenseg()
        {
        }

        public Point[] Poziciok
        {
            get
            {
                return this.poziciok;
            }

            set
            {
                this.poziciok = value;
            }
        }

        public int EllensegIndex
        {
            get
            {
                return this.ellensegIndex;
            }

            set
            {
                this.ellensegIndex = value;
            }
        }

        /// <summary>
        /// Az ellenfelek mozgása.
        /// Csak egy mezőt lépnek a megadtt irányba, ha nincs ott fal
        /// </summary>
        /// <param name="dx">vízszintes elmozdulás</param>
        /// <param name="dy">függőleges elmozdulás</param>
        /// <param name="ellenseg">Az ellenségek</param>
        /// <param name="falak">A falak</param>
        public void EllensegMozog(int dx, int dy, Point[] ellenseg, bool[,] falak)
        {
            for (int i = 0; i < ellenseg.Length; i++)
            {
                if (ellenseg[i].Y + dy >= falak.GetLength(0) ||
                    ellenseg[i].Y + dy < 0 ||
                    ellenseg[i].X + dx >= falak.GetLength(1) ||
                    ellenseg[i].X + dx < 0 ||
                    !falak[(int)ellenseg[i].Y + dy, (int)ellenseg[i].X + dx])
                {
                    return;
                }

                ellenseg[i] = new Point(ellenseg[i].X + dx, ellenseg[i].Y + dy);
            }
        }
    }
}
