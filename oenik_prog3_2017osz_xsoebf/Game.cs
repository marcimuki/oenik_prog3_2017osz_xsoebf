﻿namespace oenik_prog3_2017osz_xsoebf
{
    using System;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public class Game
    {
        public const int MEZO = 10;
        private bool[,] falak;
        private bool[,] talaj;
        private Point cel;
        private Point hatralevoLepesek;
        private Point hpMutato;
        private int palyaszam;
        private int lepesek;

        public bool[,] Falak
        {
            get
            {
                return this.falak;
            }

            set
            {
                this.falak = value;
            }
        }

        public bool[,] Talaj
        {
            get
            {
                return this.talaj;
            }

            set
            {
                this.talaj = value;
            }
        }

        public Point Cel
        {
            get
            {
                return this.cel;
            }

            set
            {
                this.cel = value;
            }
        }

        public Point HatralevoLepesek
        {
            get
            {
                return this.hatralevoLepesek;
            }

            set
            {
                this.hatralevoLepesek = value;
            }
        }

        public Point HpMutato
        {
            get
            {
                return this.hpMutato;
            }

            set
            {
                this.hpMutato = value;
            }
        }

        public int Palyaszam
        {
            get
            {
                return this.palyaszam;
            }

            set
            {
                this.palyaszam = value;
            }
        }

        public int Lepesek
        {
            get
            {
                return this.lepesek;
            }

            set
            {
                this.lepesek = value;
            }
        }

        /// <summary>
        /// Képekből készít Brush-t
        /// </summary>
        /// <param name="big">true = nagy 2*2 mezős, false = 1*1 mezős</param>
        /// <param name="path">fájl elérési útja</param>
        /// <returns>ImageBrush</returns>
        public ImageBrush GetImageBrush(bool big, string path)
        {
            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(path, UriKind.Relative)));
            ib.TileMode = TileMode.Tile;
            if (big == true)
            {
                ib.Viewport = new Rect(0, 0, Game.MEZO * 2, Game.MEZO * 2);
            }
            else if (big == false)
            {
                ib.Viewport = new Rect(0, 0, Game.MEZO, Game.MEZO);
            }

            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }
    }
}
