﻿namespace oenik_prog3_2017osz_xsoebf
{
    using System.Windows;

    public class Jatekos
    {
        private Point alak;
        private int hp;

        public Jatekos()
        {
            this.hp = 3;
        }

        public Point Alak
        {
            get
            {
                return this.alak;
            }

            set
            {
                this.alak = value;
            }
        }

        public int Hp
        {
            get
            {
                return this.hp;
            }

            set
            {
                this.hp = value;
            }
        }
    }
}
