﻿
namespace oenik_prog3_2017osz_xsoebf
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    public class PointToGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Pointból, vagy Point mátrixból Geometrybe alakít
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>Ha Point mátrix a forrás (ellenségek) akkor GeometryGroup, különben RectangleGeometry</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is Point[])
            {
                Point[] p = new Point[((Point[])value).Length];
                GeometryGroup gg = new GeometryGroup();
                int pindex = 0;
                foreach (Point ellensegek in (Point[])value)
                {
                    p[pindex] = (Point)ellensegek;
                    gg.Children.Add(new RectangleGeometry(new Rect(p[pindex].X * Game.MEZO, p[pindex].Y * Game.MEZO, Game.MEZO, Game.MEZO)));
                    pindex++;
                }

                return gg;
            }
            else
            {
                    Point p = (Point)value;
                    return new RectangleGeometry(new Rect(p.X * Game.MEZO, p.Y * Game.MEZO, Game.MEZO, Game.MEZO));
                //else
                //{
                //    Jatekos j = new Jatekos();
                //    j = (Jatekos)value;
                //    Point p = j.Pozicio;
                //    return new RectangleGeometry(new Rect(p.X * ViewModel.MEZO, p.Y * ViewModel.MEZO, ViewModel.MEZO, ViewModel.MEZO));
                //}

            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
