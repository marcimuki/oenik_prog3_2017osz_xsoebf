﻿namespace oenik_prog3_2017osz_xsoebf
{
    using System;
    using System.Globalization;
    using System.Windows;
    using System.Windows.Data;
    using System.Windows.Media;

    public class PointToBigGeometryConverter : IValueConverter
    {
        /// <summary>
        /// Pointból Geometrybe alakít
        /// </summary>
        /// <param name="value">value</param>
        /// <param name="targetType">targetType</param>
        /// <param name="parameter">parameter</param>
        /// <param name="culture">culture</param>
        /// <returns>2 * 2 mezős RectangleGeometry</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Point p = (Point)value;
            return new RectangleGeometry(new Rect(p.X * Game.MEZO, p.Y * Game.MEZO, Game.MEZO * 2, Game.MEZO * 2));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
