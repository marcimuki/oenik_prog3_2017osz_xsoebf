﻿/// <copyright file="ViewModel.cs" company="PlaceholderCompany">
/// Copyright (c) PlaceholderCompany. All rights reserved.
/// </copyright>

namespace oenik_prog3_2017osz_xsoebf
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    public class ViewModel : INotifyPropertyChanged
    {
        private Jatekos jatekos;
        private Ellenseg ellenseg;
        private Game game;

        public ViewModel()
        {
            this.game = new Game();
            this.ellenseg = new Ellenseg();
            this.jatekos = new Jatekos();
            this.jatekos.Alak = new Point(4, 6);
            this.game.Palyaszam = 1;
            this.CelbaErt += this.ViewModel_CelbaErt;
            this.Megserult += this.ViewModel_Megserult;
            this.Lepett += this.ViewModel_Lepett;
            this.game.Falak = new bool[7, 9];
            this.game.Talaj = new bool[7, 9];
            this.ellenseg.Poziciok = new Point[4];
            this.ellenseg.EllensegIndex = 0;
            this.PalyaBetolt();
            this.game.Lepesek = 8;
        }

        public event EventHandler CelbaErt;

        public event EventHandler Megserult;

        public event EventHandler Lepett;

        public event PropertyChangedEventHandler PropertyChanged;

        public bool[,] Falak
        {
            get { return this.game.Falak; }
        }

        public bool[,] Talaj
        {
            get { return this.game.Talaj; }
        }

        public Point Jatekos
        {
            get
            {
                return this.jatekos.Alak;
            }

            set
            {
                this.jatekos.Alak = value;
                this.OPC();
            }
        }

        public Point HpMutato
        {
            get
            {
                return this.game.HpMutato;
            }

            set
            {
                this.game.HpMutato = value;
                this.OPC();
            }
        }

        public int Hp
        {
            get
            {
                return this.jatekos.Hp;
            }

            set
            {
                this.jatekos.Hp = value;
                this.OPC();
            }
        }

        public Point HatralevoLepesek
        {
            get
            {
                return this.game.HatralevoLepesek;
            }

            set
            {
                this.game.HatralevoLepesek = value;
                this.OPC();
            }
        }

        public Point[] Ellenseg
        {
            get
            {
                return this.ellenseg.Poziciok;
            }

            set
            {
                this.ellenseg.Poziciok = value;
                this.OPC();
            }
        }

        public Point Cel
        {
            get { return this.game.Cel; }
            set { this.game.Cel = value; }
        }

        public ImageBrush WallBrush
        {
            get { return this.game.GetImageBrush(false, "_wall.bmp"); }
        }

        public ImageBrush FloorBrush
        {
            get { return this.game.GetImageBrush(false, "_floor.bmp"); }
        }

        /// <summary>
        /// Gets a hátralévő lépéseknek megfelelő képet ad vissza
        /// </summary>
        public ImageBrush NumberBrush
        {
            get
            {
                if (this.Lepesek >= 1)
                {
                    return this.game.GetImageBrush(true, "_" + this.Lepesek + ".bmp");
                }
                else if (this.Lepesek < 1)
                {
                    return this.game.GetImageBrush(true, "_1.bmp");
                }
                else
                {
                    return null;
                }
            }
        }

        public ImageBrush HealthBrush
        {
            get { return this.game.GetImageBrush(true, "_hp" + this.jatekos.Hp + ".bmp"); }
        }

        public ImageBrush PlayerBrush
        {
            get { return this.game.GetImageBrush(false, "_player.bmp"); }
        }

        public ImageBrush ExitBrush
        {
            get { return this.game.GetImageBrush(false, "_cel.bmp"); }
        }

        public ImageBrush EnemyBrush
        {
            get { return this.game.GetImageBrush(false, "_enemy.bmp"); }
        }

        public int Lepesek
        {
            get
            {
                return this.game.Lepesek;
            }

            set
            {
                this.game.Lepesek = value;
                this.OPC();
            }
        }

        /// <summary>
        /// Feldob egy MessageBoxot, amin az Ok-ra kattintáskor bezárja a játékot.
        /// </summary>
        public void GameOver()
        {
            MessageBoxResult result = MessageBox.Show("JÁTÉK VÉGE", "JÁTÉK VÉGE", MessageBoxButton.OK);
            switch (result)
            {
                case MessageBoxResult.OK:
                    System.Windows.Application.Current.Shutdown();
                    break;
            }
        }

        /// <summary>
        /// A játékos mozgása. Addig megy a megadott irányba, míg falnak nem ütközik.
        /// Ha nekimegy egy ellenfélnek, vagy elfogynak a lépései ellövi a Megsérül eseményt.
        /// Ha beér a célba, ellövi a Célbaért eseményt.
        /// </summary>
        /// <param name="dx">vízszintes elmozdulás</param>
        /// <param name="dy">függőleges elmozdulás</param>
        public void JatekosMozog(int dx, int dy)
        {
            if (this.Lepett != null)
            {
                this.Lepett(this, EventArgs.Empty);
            }

            if (this.game.Lepesek < 0)
            {
                if (this.Megserult != null)
                {
                    this.Megserult(this, EventArgs.Empty);
                    return;
                }
            }

            for (int i = 0; i < 10; i++)
            {
                if (this.Jatekos.Y + dy >= this.game.Falak.GetLength(0) ||
                    this.Jatekos.Y + dy < 0 ||
                    this.Jatekos.X + dx >= this.game.Falak.GetLength(1) ||
                    this.Jatekos.X + dx < 0 ||
                    !this.Falak[(int)this.Jatekos.Y + dy, (int)this.Jatekos.X + dx])
                {
                    return;
                }

                this.Jatekos = new Point(this.Jatekos.X + dx, this.Jatekos.Y + dy);

                for (int j = 0; j < this.ellenseg.EllensegIndex; j++)
                {
                    if (this.Jatekos == this.Ellenseg[j])
                    {
                        if (this.Megserult != null)
                        {
                            this.Megserult(this, EventArgs.Empty);
                            return;
                        }
                    }
                }

                if (this.Jatekos == this.Cel)
                {
                    if (this.CelbaErt != null)
                    {
                        this.CelbaErt(this, EventArgs.Empty);
                        return;
                    }
                }
            }
        }

        public void EllensegMozog(int dx, int dy)
        {
            this.ellenseg.EllensegMozog(dx, dy, this.ellenseg.Poziciok, this.game.Falak);
        }

        private void OPC([CallerMemberName]string n = "")
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(n));
            }
        }

        /// <summary>
        /// Minden kurzormozgató billentyű lenyomásnál. (akkor is, ha a karakter nem mozdul el)
        /// Levon egyet a hátralévő lépésekből
        /// </summary>
        /// <param name="sender">value</param>
        /// <param name="e">targetType</param>
        private void ViewModel_Lepett(object sender, EventArgs e)
        {
            this.Lepesek--;
            this.OPC("NumberBrush");
            this.OPC("Ellenseg");
            this.OPC("EnemyBrush");
            this.OPC("Falak");
            this.OPC("Jatekos");
            this.OPC("Cel");
            this.OPC("Talaj");
            this.OPC("PlayerBrush");
            this.OPC("HealthBrush");
            this.OPC("Hp");
        }

        /// <summary>
        /// Ha a játékos beért a célba
        /// Élete, és lépései feltöltődik, meghívja a következő pálya betöltését
        /// Ha elérte az 5. pályát, meghívja a jááték végét.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void ViewModel_CelbaErt(object sender, EventArgs e)
        {
            if (++this.game.Palyaszam <= 3)
            {
                this.jatekos.Hp = 3;
                this.game.Lepesek = 8;
                this.ellenseg.EllensegIndex = 0;
                this.PalyaBetolt();
                this.OPC("Falak");
                this.OPC("Jatekos");
                this.OPC("Cel");
                this.OPC("Talaj");
                this.OPC("Ellenseg");
                this.OPC("NumberBrush");
                this.OPC("HealthBrush");
                this.OPC("Hp");
            }
            else
            {
                this.GameOver();
            }
        }

        /// <summary>
        /// Ha a karakter nekimegy egy ellenfélnek, vagy elfogynak a lépései.
        /// Lépések újraindulnak.
        /// Ha elfogy a karakter élete, meghívja a játék végét.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">e</param>
        private void ViewModel_Megserult(object sender, EventArgs e)
        {
            this.game.Lepesek = 8;
            this.ellenseg.EllensegIndex = 0;
            this.PalyaBetolt();
            this.OPC("Falak");
            this.OPC("Jatekos");
            this.OPC("Cel");
            this.OPC("Talaj");
            this.OPC("Ellenseg");
            this.OPC("NumberBrush");
            this.jatekos.Hp--;
            if (this.jatekos.Hp == 0)
            {
                this.GameOver();
            }

            this.OPC("Hp");
            this.OPC("HealthBrush");
        }

        /// <summary>
        /// Betölti az aktuális pályát a txt fájlok alapján.
        /// Attól függően, hogy a fájl adott sorában és oszlopában miylen karakter van,
        /// a játéktérre létrehoz egy pointot, vagy a bool mátrixba falat/padlót tesz.
        /// f - fal
        /// u - út
        /// m - lépésszámláló
        /// h - életszámláló
        /// j - játékos
        /// c - cél
        /// e - ellenfelek
        /// </summary>
        private void PalyaBetolt()
        {
            string[] lines = System.IO.File.ReadAllLines("lvl" + this.game.Palyaszam + ".lvl");
            for (int i = 0; i < lines.Length; i++)
            {
                for (int j = 0; j < lines[i].Length; j++)
                {
                    if (lines[i][j] == 'f')
                    {
                        this.game.Falak[i, j] = false;
                        this.game.Talaj[i, j] = true;
                    }
                    else if (lines[i][j] == 'm')
                    {
                        this.game.HatralevoLepesek = new Point(j, i);
                        this.game.Falak[i, j] = true;
                        this.game.Talaj[i, j] = true;
                    }
                    else if (lines[i][j] == 'h')
                    {
                        this.game.HpMutato = new Point(j, i);
                        this.game.Falak[i, j] = true;
                        this.game.Talaj[i, j] = true;
                    }
                    else if (lines[i][j] == 'j')
                    {
                        this.jatekos.Alak = new Point(j, i);
                        this.game.Falak[i, j] = true;
                        this.game.Talaj[i, j] = false;
                    }
                    else if (lines[i][j] == 'u')
                    {
                        this.game.Talaj[i, j] = false;
                        this.game.Falak[i, j] = true;
                    }
                    else if (lines[i][j] == 'u' || lines[i][j] == 'j')
                    {
                        this.game.Falak[i, j] = true;
                    }
                    else if (lines[i][j] == 'c')
                    {
                        this.game.Cel = new Point(j, i);
                        this.game.Falak[i, j] = true;
                        this.game.Talaj[i, j] = false;
                    }
                    else if (lines[i][j] == 'e')
                    {
                        this.ellenseg.Poziciok[this.ellenseg.EllensegIndex] = new Point(j, i);
                        this.ellenseg.EllensegIndex++;
                        this.game.Falak[i, j] = true;
                        this.game.Talaj[i, j] = false;
                    }
                }
            }
        }
    }
}
